# language: pt
# encoding: UTF-8
@deletar_simulacao
Funcionalidade: Validar a delecao de simulacao ja criada

  Esquema do Cenario: Efetuar o delete de simulacao ja criada com sucesso
    Dado que informo o "<id>"
    Quando executo a API 'Deletar-Simulacao' com os parametros informados
    Entao e realizado a delecao da simulacao existente com sucesso

    Exemplos: 
      | id |
      | 13 |
