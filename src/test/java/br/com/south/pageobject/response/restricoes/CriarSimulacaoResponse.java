package br.com.south.pageobject.response.restricoes;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CriarSimulacaoResponse {

	@JsonProperty ("nome")
	private String nome;
	
	@JsonProperty ("cpf")
	private String cpf;
	
	@JsonProperty ("email")
	private String email;
	
	@JsonProperty ("valor")
	private Integer valor;
	
	@JsonProperty ("parcelas")
	private Integer parcelas;
	
	public Integer getParcelas() {
		return parcelas;
	}

	public void setParcelas(Integer parcelas) {
		this.parcelas = parcelas;
	}

	@JsonProperty ("seguro")
	private boolean seguro;
	
	@JsonProperty ("id")
	private Integer id;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Number getValor() {
		return valor;
	}

	public void setValor(Integer valor) {
		this.valor = valor;
	}

	public boolean isSeguro() {
		return seguro;
	}

	public void setSeguro(boolean seguro) {
		this.seguro = seguro;
	}
	
	
}
