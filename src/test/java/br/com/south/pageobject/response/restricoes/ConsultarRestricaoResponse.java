/**
 * 
 */
package br.com.south.pageobject.response.restricoes;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Dionatan
 *
 */
public class ConsultarRestricaoResponse {
	
	@JsonProperty ("mensagem")
	private String mensagem;

	public String getMensagemRetorno() {
		return mensagem;
	}

	public void setMensagemRetorno(String mensagem) {
		this.mensagem = mensagem;
	}
	

}
