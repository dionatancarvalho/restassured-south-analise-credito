package br.com.south.pageobject.response.restricoes;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DeletarSimulacaoResponse {

	@JsonProperty("id")
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
