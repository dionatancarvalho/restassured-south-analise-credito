package br.com.south.pageobject.request.restricoes;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DeletarSimulacaoResquest {

	@JsonProperty ("id")
	private int id;
	
	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}
}
