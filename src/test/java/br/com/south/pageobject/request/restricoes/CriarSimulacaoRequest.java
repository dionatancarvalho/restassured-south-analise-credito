package br.com.south.pageobject.request.restricoes;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CriarSimulacaoRequest {

	
	@JsonProperty ("nome")
	private String nome;
	
	@JsonProperty ("cpf")
	private String cpf;
	
	@JsonProperty ("email")
	private String email;
	
	@JsonProperty ("valor")
	private Integer valor;
	
	@JsonProperty ("parcelas")
	private Integer parcelas;
	
	@JsonProperty ("seguro")
	private boolean seguro;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getValor() {
		return valor;
	}

	public void setValor(Integer valor) {
		this.valor = valor;
	}

	public Integer getParcelas() {
		return parcelas;
	}

	public void setParcelas(Integer parcelas) {
		this.parcelas = parcelas;
	}

	public boolean isSeguro() {
		return seguro;
	}

	public void setSeguro(boolean seguro) {
		this.seguro = seguro;
	}

	
	
}
