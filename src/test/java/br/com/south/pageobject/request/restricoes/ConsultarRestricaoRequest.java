package br.com.south.pageobject.request.restricoes;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ConsultarRestricaoRequest {
	
	@JsonProperty("cpf")
	private String cpf;

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

}
