package br.com.south.pageobject.request.restricoes;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AlterarSimulacaoExistenteRequest {

	@JsonProperty ("nome")
	private String nome;
	
	@JsonProperty ("cpf")
	private String cpf;
	
	@JsonProperty ("email")
	private String email;
	
	@JsonProperty ("valor")
	private int valor;
	
	@JsonProperty ("parcelas")
	private String parcelas;
	
	@JsonProperty ("seguro")
	private boolean seguro;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getValor() {
		return valor;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}

	public String getParcelas() {
		return parcelas;
	}

	public void setParcelas(String parcelas) {
		this.parcelas = parcelas;
	}

	public boolean isSeguro() {
		return seguro;
	}

	public void setSeguro(boolean seguro) {
		this.seguro = seguro;
	}
	
}
