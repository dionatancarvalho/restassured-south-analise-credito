package br.com.south.steps;

import br.com.south.enuns.URLsAPIs;
import br.com.south.pageobject.request.restricoes.CriarSimulacaoRequest;
import br.com.south.pageobject.response.restricoes.CriarSimulacaoResponse;
import io.restassured.http.ContentType;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import static org.junit.Assert.assertNotNull;

public class CriarSimulacaoSteps {
	
	private CriarSimulacaoResponse response;
	
	@Step("Executar API 'Criar-Simulacao'")
	
	public void executarAPICriarSimulacao(CriarSimulacaoRequest criarSimulacao) {
		response=SerenityRest.
			given().
				relaxedHTTPSValidation().
				//proxy(URL_PROXY).				//Caso exija configuracao de proxy
				//auth().						//Caso exija autenticacao
				//preemptive().					//Caso exija autenticacao
				//oauth2(new TokenGenerator().obterToken()).		//Caso exija autenticacao
				accept("*/*").
				contentType(ContentType.JSON).
				body(criarSimulacao).
				log().
				all().
			when().
				log().
				all().
				post(URLsAPIs.API_SIMULACOES.getUrl()).
			then().	
				log().
				all().
			assertThat().
				statusCode(201).
				extract().
				as(CriarSimulacaoResponse.class);
	
	}
	
	@Step ("e realizado a criacao da simulacao com sucesso'$")
	public void validarResponseCriarSimulacao(){
		assertNotNull(response);
	}
}
