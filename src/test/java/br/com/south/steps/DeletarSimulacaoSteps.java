package br.com.south.steps;

import static org.junit.Assert.assertNotNull;

import br.com.south.enuns.URLsAPIs;
import br.com.south.pageobject.request.restricoes.DeletarSimulacaoResquest;
import br.com.south.pageobject.response.restricoes.DeletarSimulacaoResponse;
import io.restassured.http.ContentType;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class DeletarSimulacaoSteps {
	
	private DeletarSimulacaoResponse response;
	
	@Step("Executar API 'Deletar-Simulacao")
	
	public void executarAPIDeletarSimulacao(DeletarSimulacaoResquest deletarSimulacao) {
		
		String id = "23";
		response=SerenityRest.
				given().
				pathParams("id", id).
				relaxedHTTPSValidation().
				//proxy(URL_PROXY).				//Caso exija configuracao de proxy
				//auth().						//Caso exija autenticacao
				//preemptive().					//Caso exija autenticacao
				//oauth2(new TokenGenerator().obterToken()).		//Caso exija autenticacao
				accept("*/*").
	            contentType(ContentType.TEXT).
				body(deletarSimulacao).
				log().
				all().
			when().
				log().
				all().
				delete(URLsAPIs.API_SIMULACOES.getUrl() + "/{id}").
			then().	
				log().
				all().
			assertThat().
				statusCode(200).
				extract().
				as(DeletarSimulacaoResponse.class);
	
		}
	
	@Step ("e realizado a alteracao da simulacao com sucesso'$")
	public void validarResponseDeletarSimulacao(){
	assertNotNull(response);
		
		}
	}
