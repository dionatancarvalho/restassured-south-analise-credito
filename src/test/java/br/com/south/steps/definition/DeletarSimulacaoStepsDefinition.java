package br.com.south.steps.definition;

import br.com.south.pageobject.request.restricoes.DeletarSimulacaoResquest;
import br.com.south.steps.DeletarSimulacaoSteps;
import cucumber.api.java.Before;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DeletarSimulacaoStepsDefinition {
	
	@Steps
	private DeletarSimulacaoSteps steps;
	private DeletarSimulacaoResquest deletarSimulacao;
	
	
	@Before
	public void setUp() {
		deletarSimulacao = new DeletarSimulacaoResquest();
		
	}
	@Dado("^que informo o \"([^\"]*)\"$")
	public void queInformoOID(Integer id) {
		deletarSimulacao.setId(id);
	}
	
	@Quando ("^executo a API 'Deletar-Simulacao' com os parametros informados$")
	public void queExecutoAAPIComParametrosInformados() {
		steps.executarAPIDeletarSimulacao(deletarSimulacao);
	}
	
	@Entao("^e realizado a delecao da simulacao existente com sucesso$")
	public void validoADelecaoComSucesso() {
		steps.validarResponseDeletarSimulacao();
	}
}
