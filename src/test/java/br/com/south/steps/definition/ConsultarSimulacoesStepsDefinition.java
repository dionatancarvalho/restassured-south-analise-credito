package br.com.south.steps.definition;

import br.com.south.pageobject.request.simulacao.ConsultarSimulacoesRequest;
import br.com.south.steps.ConsultarSimulacoesSteps;
import cucumber.api.java.Before;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class ConsultarSimulacoesStepsDefinition {

	
	@Steps
	private ConsultarSimulacoesSteps steps;
	private ConsultarSimulacoesRequest consultarSimulacoes;
	
	@Before
	public void setUp() {
		consultarSimulacoes = new ConsultarSimulacoesRequest();
	}

	@Dado("^que eu gostaria de consultar todas as simulacoes cadastradas$")
	public void queGostariaDeConnsultarTodasAsSimulacoesCadastradas() {
	}
	
	@Quando("^executo a API 'Consultar-Simulacoes' com os parametros informados$")
	public void executoAPIComParametrosInformados() {
	    steps.executarAPIConsultarSimulacoes(consultarSimulacoes);
	}
	
	@Entao("^valido o retorno da API 'Consultar-Simulacoes' com as simulacoes cadastradas$")
	public void validoORetornoDaAPI() { 
	    steps.validarResponseConsultarSimulacoes();
	}
	
}
