package br.com.south.steps.definition;

import br.com.south.pageobject.request.restricoes.AlterarSimulacaoExistenteRequest;
import br.com.south.steps.AlterarSimulacaoExistenteSteps;
import cucumber.api.java.Before;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class AlterarSimulacaoExistenteStepsDefinition {
	@Steps
	private AlterarSimulacaoExistenteSteps steps;
	private AlterarSimulacaoExistenteRequest alterarSimulacao;
	
	
	@Before
	public void setUp() {
		alterarSimulacao = new AlterarSimulacaoExistenteRequest();
		
	}
	
	@Dado("^que informo o numero do Cpf com \"([^\"]*)\"$")
	public void queInformoONumeroCPF(String numeroCpf) {
		alterarSimulacao.setCpf(numeroCpf);
	}
	
	@E ("^informo o nome com \"([^\"]*)\"$")
	public void queInformoONome(String nome) {
		alterarSimulacao.setNome(nome);
		
	}
	
	@E ("^informo o email com \"([^\"]*)\"$")
	public void queInformoOEmail(String email) {
		alterarSimulacao.setEmail(email);
	}
	
	@E ("^informo o valor com \"([^\"]*)\"$")
	public void queInnformoOValor(Integer valor) {
		alterarSimulacao.setValor(valor);
	}
	
	@E ("^informo a parcela com \"([^\"]*)\"$")
	public void queInformoAParcela(String parcela) {
		alterarSimulacao.setParcelas(parcela);
	}
	
	@E ("^se tera seguro \"([^\"]*)\"$")
	public void queInformoSeTemSeguro (Boolean seguro) {
		alterarSimulacao.setSeguro(seguro);
	}
	
	@Quando ("^executo a API 'Alterar-Simulacao' com os parametros informados$")
	public void queExecutoAAPIComParametrosInformados() {
		steps.executarAPIAlterarSimulacao(alterarSimulacao);
	}
	
	@Entao("^e realizado a alteracao da simulacao existente com sucesso$")
	public void validoORetornoComSucesso() {
		steps.validarResponseAlterarSimulacao();
	}

}
