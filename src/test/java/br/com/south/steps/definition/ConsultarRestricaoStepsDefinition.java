package br.com.south.steps.definition;

import br.com.south.pageobject.request.restricoes.ConsultarRestricaoRequest;
import br.com.south.steps.ConsultarRestricaoSteps;
import cucumber.api.java.Before;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class ConsultarRestricaoStepsDefinition {
	
	@Steps
	private ConsultarRestricaoSteps steps;
	private ConsultarRestricaoRequest consultarRestricao;
	
	@Before
	public void setUp() {
		consultarRestricao = new ConsultarRestricaoRequest();
		
	}

	@Dado("^que informo o numero do Cpf \"([^\"]*)\"$")
		public void queInformoONumeroCPF(String numeroCpf) {
		consultarRestricao.setCpf(numeroCpf);
		
	}
	
	@Quando("^executo a API 'Consultar-Restricao' com os parametros informados$")
	public void executoAPIComParametrosInformados() {
	    steps.executarAPIConsultarRestricao(consultarRestricao);
	}
	
	@Entao("^valido o retorno da API 'Consultar-Restricao'$")
	public void validoORetornoDaAPI() {
	    steps.validarResponseConsultarRestricao();
	}

}
