package br.com.south.steps.definition;

import br.com.south.pageobject.request.restricoes.CriarSimulacaoRequest;
import br.com.south.steps.CriarSimulacaoSteps;
import cucumber.api.java.Before;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class CriarSimulacaoStepsDefinition {

	
	@Steps
	private CriarSimulacaoSteps steps;
	private CriarSimulacaoRequest criarSimulacao;
	
	@Before
	public void setUp() {
		criarSimulacao = new CriarSimulacaoRequest();
		
	}

	
	@Dado("^que informo o numero do \"([^\"]*)\"$")
	public void queInformoONumeroCPF(String numeroCpf) {
		criarSimulacao.setCpf(numeroCpf);
	
	}
	
	@E ("^informo o nome \"([^\"]*)\"$")
	public void queInformoONome(String nome) {
		criarSimulacao.setNome(nome);
		
	}
	
	@E ("^informo o email \"([^\"]*)\"$")
	public void queInformoOEmail(String email) {
		criarSimulacao.setEmail(email);
	}
	
	@E ("^informo o valor \"([^\"]*)\"$")
	public void queInnformoOValor(Integer valor) {
		criarSimulacao.setValor(valor);
	}
	
	@E ("^informo a parcela \"([^\"]*)\"$")
	public void queInformoAParcela(Integer parcela) {
		criarSimulacao.setParcelas(parcela);
	}
	
	@E ("^se tera seguro com \"([^\"]*)\"$")
	public void queInformoSeTemSeguro (Boolean seguro) {
		criarSimulacao.setSeguro(seguro);
	}
	
	@Quando ("^executo a API 'Criar-Simulacao' com os parametros informados$")
	public void queExecutoAAPIComParametrosInformados() {
		steps.executarAPICriarSimulacao(criarSimulacao);
	}
	
	@Entao("^e realizado a criacao da simulacao com sucesso$")
	public void validoORetornoComSucesso() {
		steps.validarResponseCriarSimulacao();
	}
	
	@Entao ("^e retornado mensagem de erro email$")
	public void validoRetornoEmailComErro() {
		
	}
	
	@Entao("^e retornado a mensagem de cpf ja existente$")
	public void validoORetornoComErro() {
		steps.validarResponseCriarSimulacao();
	}
}	

