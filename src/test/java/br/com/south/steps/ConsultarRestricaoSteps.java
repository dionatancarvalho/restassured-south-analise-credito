package br.com.south.steps;


import static org.junit.Assert.assertNotNull;
import br.com.south.enuns.URLsAPIs;
import br.com.south.pageobject.request.restricoes.ConsultarRestricaoRequest;
import br.com.south.pageobject.response.restricoes.ConsultarRestricaoResponse;
import io.restassured.http.ContentType;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;


public class ConsultarRestricaoSteps {

	private ConsultarRestricaoResponse response;

	
	@Step("executo a API 'Consultar-Restricao' com os parametros informados$")
	
	
		public void executarAPIConsultarRestricao(ConsultarRestricaoRequest consultarRestricao) {
		String cpf = "97093236014";
		response=SerenityRest.
				given().
				pathParams("cpf", cpf).
				relaxedHTTPSValidation().
				//proxy(URL_PROXY).				//Caso exija configuracao de proxy
				//auth().						//Caso exija autenticacao
				//preemptive().					//Caso exija autenticacao
				//oauth2(new TokenGenerator().obterToken()).		//Caso exija autenticacao
				accept("*/*").
				contentType(ContentType.JSON).
				body(consultarRestricao).
				log().
				all().
			when().
				log().
				all().
				get(URLsAPIs.API_RESTRICOES.getUrl() + "/{cpf}").	
			then().	
				log().
				all().
			assertThat().
					statusCode(200).
					extract().
					as(ConsultarRestricaoResponse.class);
		
	}
		

	@Step ("valido o retorno da API 'Consultar-Restricao'$")
	public void validarResponseConsultarRestricao(){
	assertNotNull(response);
	

	//implementar mais asserts de acordo com a necessidade do teste
	}
	
}
