package br.com.south.steps;

import static org.junit.Assert.assertNotNull;

import br.com.south.enuns.URLsAPIs;
import br.com.south.pageobject.request.simulacao.ConsultarSimulacoesRequest;
import br.com.south.pageobject.response.simulacao.ConsultarSimulacoesResponse;
import io.restassured.http.ContentType;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class ConsultarSimulacoesSteps {
	private ConsultarSimulacoesResponse[] response;

	
	@Step("executo a API 'Consultar-Simulacoes' com os parametros informados$")
	
	
		public void executarAPIConsultarSimulacoes(ConsultarSimulacoesRequest consultarSimulacao) {
		response=SerenityRest.
				given().
				relaxedHTTPSValidation().
				//proxy(URL_PROXY).				//Caso exija configuracao de proxy
				//auth().						//Caso exija autenticacao
				//preemptive().					//Caso exija autenticacao
				//oauth2(new TokenGenerator().obterToken()).		//Caso exija autenticacao
				accept("*/*").
				contentType(ContentType.JSON).
				//body(consultarSimulacao).
				log().
				all().
			when().
				log().
				all().
				get(URLsAPIs.API_SIMULACOES.getUrl()).	
			then().	
				log().
				all().
			assertThat().
				statusCode(200).
				extract().
				as(ConsultarSimulacoesResponse[].class);
		
	}
		

	@Step("valido o retorno da API 'Consultar-Simulacoes' com as simulacoes cadastradas$")
	public void validarResponseConsultarSimulacoes() {
		assertNotNull(response);

		// implementar mais asserts de acordo com a necessidade do teste
	}
}
