package br.com.south.steps;

import static org.junit.Assert.assertNotNull;

import br.com.south.enuns.URLsAPIs;
import br.com.south.pageobject.request.restricoes.AlterarSimulacaoExistenteRequest;
import br.com.south.pageobject.response.restricoes.AlterarSimulacaoExistenteResponse;
import io.restassured.http.ContentType;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class AlterarSimulacaoExistenteSteps {
	
	private AlterarSimulacaoExistenteResponse response;
	
	@Step("Executar API 'Alterar-Simulacao")
	
	public void executarAPIAlterarSimulacao(AlterarSimulacaoExistenteRequest alterarSimulacao) {
		
		String cpf = "48744605773";
		response=SerenityRest.
				given().
				pathParams("cpf", cpf).
				relaxedHTTPSValidation().
				//proxy(URL_PROXY).				//Caso exija configuracao de proxy
				//auth().						//Caso exija autenticacao
				//preemptive().					//Caso exija autenticacao
				//oauth2(new TokenGenerator().obterToken()).		//Caso exija autenticacao
				accept("*/*").
				contentType(ContentType.JSON).
				body(alterarSimulacao).
				log().
				all().
			when().
				log().
				all().
				put(URLsAPIs.API_SIMULACOES.getUrl() + "/{cpf}").
			then().	
				log().
				all().
			assertThat().
				statusCode(200).
				extract().
				as(AlterarSimulacaoExistenteResponse.class);
	
		}
	
	@Step ("e realizado a alteracao da simulacao com sucesso'$")
	public void validarResponseAlterarSimulacao(){
	assertNotNull(response);
		
		}
	}

