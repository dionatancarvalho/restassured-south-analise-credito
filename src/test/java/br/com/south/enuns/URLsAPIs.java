package br.com.south.enuns;

public enum URLsAPIs {

	//API_TOKEN_CREDITO("https://api-digitaluat.southsystem.com.br/auth/oauth/v2/token"), //URL de API de autenticacao
	API_SIMULACOES ("http://localhost:8888/api/v1/simulacoes"),
	API_RESTRICOES("http://localhost:8888/api/v1/restricoes");


	
private final String url;
	
	URLsAPIs(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}		
}
	




