package br.com.south.runner;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
		features = "classpath:features",
		plugin = { "pretty"},
		monochrome = true,
		tags = "@criar_simulacao",
		glue = {"br.com.south.steps.definition"},
		dryRun = false)

public class CriarSimulacaoRunner {

}
