# language: pt
# encoding: UTF-8
@criar_simulacao
Funcionalidade: Validar a criacao da request da api

  Esquema do Cenario: Efetuar a criacao de simulacao com sucesso
    Dado que informo o numero do "<cpf>"
    E informo o nome "<nome>"
    E informo o email "<email>"
    E informo o valor "<valor>"
    E informo a parcela "<parcela>"
    E se tera seguro com "<seguro>"
    Quando executo a API 'Criar-Simulacao' com os parametros informados
    Entao e realizado a criacao da simulacao com sucesso

    Exemplos: 
      | cpf         | nome     | email                 | valor | parcela | seguro |
      | 48744605773 | Testedio | teste@dionatan.com.br |   120 |       4 | true   |

  Esquema do Cenario: Nao efetuar a criacao de simulacao com erro no formato do email
    Dado que informo o numero do "<cpf>"
    E informo o nome "<nome>"
    E informo o email "<email>"
    E informo o valor "<valor>"
    E informo a parcela "<parcela>"
    E se tera seguro com "<seguro>"
    Quando executo a API 'Criar-Simulacao' com os parametros informados
    Entao e retornado mensagem de erro email

    Exemplos: 
      | cpf         | nome     | email       | valor | parcela | seguro |
      | 48744605773 | Testedio | teste@teste |   120 |       4 | true   |
