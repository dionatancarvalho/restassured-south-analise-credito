# language: pt
# encoding: UTF-8
@consulta_restricao
Funcionalidade: Validar a consulta da request da api

  Esquema do Cenario: Efetuar a consulta de restricao pelo cpf
    Dado que informo o numero do Cpf "<cpf>"
    Quando executo a API 'Consultar-Restricao' com os parametros informados
    Entao valido o retorno da API 'Consultar-Restricao'

    Exemplos: 
      | cpf         |
      | 97093236014 |
      | 60094146012 |
      | 84809766080 |
      | 62648716050 |
      | 26276298085 |
      | 01317496094 |
      | 55856777050 |
      | 19626829001 |
      | 24094592008 |
      | 58063164083 |
      
    
      
      