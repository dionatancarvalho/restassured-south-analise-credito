# language: pt
# encoding: UTF-8
@consultar_simulacoes
Funcionalidade: Validar a consulta de todas as simulacoes cadastradas

  Esquema do Cenario: Efetuar a consulta de todas as simulacoes
    Dado que eu gostaria de consultar todas as simulacoes cadastradas
    Quando executo a API 'Consultar-Simulacoes' com os parametros informados
    Entao valido o retorno da API 'Consultar-Simulacoes' com as simulacoes cadastradas

    Exemplos: 
      |  |
      |  |
