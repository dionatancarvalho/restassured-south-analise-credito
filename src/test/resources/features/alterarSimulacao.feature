# language: pt
# encoding: UTF-8
@alterar_simulacao
Funcionalidade: Validar a alteracao da simulacao ja criada

  Esquema do Cenario: Efetuar a alteracao de simulacao ja criada com sucesso
    Dado que informo o numero do Cpf com "<cpf>"
    E informo o nome com "<nome>"
    E informo o email com "<email>"
    E informo o valor com "<valor>"
    E informo a parcela com "<parcela>"
    E se tera seguro "<seguro>"
    Quando executo a API 'Alterar-Simulacao' com os parametros informados
    Entao e realizado a alteracao da simulacao existente com sucesso

    Exemplos: 
      | cpf         | nome        | email                           | valor | parcela | seguro |
      | 48744605773 | Testedionat | testealterar123@dionatan.com.br |   140 |       3 | false  |
