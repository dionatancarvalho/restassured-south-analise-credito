## Restassured-south-analise-credito

Projeto para avaliação dos conhecimentos em automação de API com RestAssured e Serenity-BDD


## Requisitos do projeto:
 * Api disponível em https://github.com/rh-southsystem/Sicredi-Digital-QA
 * Executar a API com mvn clean spring-boot:run -Dserver.port=8888
 * API estará na URL (http://localhost:8888/)
 * Documentação da API http://localhost:8888/swagger-ui.html


## FRAMEWORKS:
 * PageObjects
 * PageFactory
 * Serenity version 2.1.10
 * Serenity Cucumber 1.9.51
 * Selenium 3.141.59
 * Java + Junit 4.12
 * Maven

### IDE
 * ECLIPSE -> 2019-09 R (4.13.0)

### JDK
 * JDK 8
